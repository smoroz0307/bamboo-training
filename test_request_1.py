# -*- coding: utf-8 -*-

import unittest
import requests


class UsersRetrieve(unittest.TestCase):

    r = requests.get('https://gorest.co.in/public/v1/users')
    users = r.json()['data']

    def test1_get_request_successful(self):
        self.assertEqual(self.r.status_code, 200)

    def test2_users_exists(self):
        self.assertGreater(len(self.users), 0)

    def test3_users_name_is_not_null(self):
        for user in self.users:
            self.assertIsNotNone(user['name'])

    def test4_users_status_exist(self):
        for user in self.users:
            self.assertIsNotNone(user['status'])

    def test5_users_id_exists(self):
        for user in self.users:
            self.assertIsNotNone(user['id'])

